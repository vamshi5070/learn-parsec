import Control.Monad.Except

type Err = String

safeDiv :: Int -> Int -> Except Err Int
safeDiv _ 0 = throwError "Divide by zero"
safeDiv a b = return $ a `div` b

example :: Either Err Int
example = runExcept $ do
  x <- safeDiv 2 3
  y <- safeDiv 2 2
  return $ x + y
