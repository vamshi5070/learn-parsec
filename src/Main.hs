{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Prelude(Int,IO)
import Text.Parsec
-- import FunctionsAndTypesForParsing
import Control.Monad.Writer
import Control.Monad.State
import Control.Monad.Trans

newtype Stack a = Stack {unStack :: StateT Int (WriterT [Int] IO) a}
  deriving (Monad, MonadState Int, MonadWriter [Int], MonadIO)

foo :: Stack ()
foo = do
  put 1
  tell [2]
  liftIO $ print 3
  return ()

  
evalStack :: Stack a -> IO [Int]
evalStack m = execWriterT $ evalStateT (unStack m) 0
