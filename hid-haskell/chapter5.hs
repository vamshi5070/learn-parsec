{-# Language OverloadedStrings #-}

import Text.Read

doubleStrNumber :: String -> Maybe Int
doubleStrNumber str = (*2) <$> readMaybe str

plusStrNumbers :: String -> String -> Maybe Int
plusStrNumbers str1 str2 = (+) <$> (readMaybe str1) <*> (readMaybe str2)

