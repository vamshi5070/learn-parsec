import Control.Parallel
import Control.Parallel.Strategies
import Control.Exception
import Data.Time.Clock
import Text.Printf
import System.Environment

fib :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

main = do
  putStr "Which test do you want?: [1][2][3][4]"
  [n] <- getArgs
  let test = [test1,test2,test3,test4] !! (read n - 1)
  t0 <- getCurrentTime -- initial time
  r <- evaluate $ runEval test -- unwrap Eval monad
  printTimeSince t0 -- initial time
  print r
  printTimeSince t0 -- after evaluation time

test1 :: Eval (Integer,Integer)
test1 = do
  x <- rpar $ fib 36
  y <- rpar $ fib 35
  return (x,y)

test2 :: Eval (Integer,Integer)
test2 = do
  x <- rpar $ fib 36
  y <- rseq $ fib 35
  return (x,y)

test3 :: Eval (Integer,Integer)
test3 = do
  x <- rpar $ fib 36
  y <- rseq $ fib 35
  rseq x
  return (x,y)
  
test4 :: Eval (Integer,Integer)
test4 = do
  x <- rpar $ fib 36
  y <- rseq $ fib 35
  rseq x
  rseq y
  return (x,y)
  

printTimeSince t0 = do
  t1 <- getCurrentTime
  printf "time: %.2fs\n" ( realToFrac (diffUTCTime t1 t0) :: Double)
